from django.shortcuts import render
from .models import *
# Create your views here.
def index(request):
	response = {}
	response['posts'] = Room.objects.all()
	response['testimonial'] = Testimonial.objects.all()
	return render(request, 'index.html', response)

def about(request):
	response = {}
	return render(request, 'about.html', response)

def katalog(request):
	response = {}
	response['apt'] = Apartment.objects.prefetch_related('room_set')
	return render(request, 'katalog.html', response)

def apartment(request,aptId):
	response= {}
	response['apt'] = Apartment.objects.prefetch_related('room_set').get(id=aptId)
	return render(request, 'apartment.html', response)

def room(request,roomId):
	response= {}
	response['room'] = Room.objects.get(id=roomId)
	response['room_lain'] = Room.objects.all()
	return render(request, 'room.html', response)


def blog(request):
	response = {}
	response['blog'] = Blog.objects.all()
	return render(request, 'blog.html', response)